import logtools
from botlog import log
import json

# Get a list of all the users that sent a message, and we'll scan other messages for their pings
logtools.count_ind_authors('chatlog.json')

# Open the required files
log('Fetching authors file...')
with open('tmp_data/all_authors.json', 'r') as f:
    all_authors = json.load(f)
    f.close()
log('Fetched authors file.')

log('Opening chatlog...')
with open('tmp_data/chatlog.json', 'r') as f:
    chatlog = json.load(f)
    f.close()
log('Opened chatlog.')

# This is because we want to know how many out of how many we have completed.
total_user_count = len(all_authors)

# The target list which we will dump eventually
account_data = []

# count the mentions of every user that has sent at least a single message
for index, account in enumerate(all_authors):
    target_user = logtools.User(account)
    logtools.count_mentions(chatlog, target_user)

    # In what format the data is stored in
    tmp_dict = {
        "id": target_user.id,
        "mention_total": target_user.total_mentions
    }
    account_data.append(tmp_dict)
    log(f'Checked user {index}/{total_user_count} ')

log('Finished all users, now sorting...')

# Sorted so that we can easily see who has the most and who has the least
account_data = logtools.sort_by(account_data, logtools.by_pings)

log('Sorted, now dumping...')

with open('tmp_data/ping_counts.json', 'w') as f:
    json.dump(account_data, f)
    f.close()

log('Dumping complete, finished!')
