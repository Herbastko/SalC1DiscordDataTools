"""
This is what to call when loading the config file fails/doesn't exist
"""
from botlog import log
import json
log('COULD NOT FIND CONFIG FILE')

config = {
    "graph_gap": 4,
    "graph_dpi": 4500,
    "get_past_count": 2592000,
    "user_graph_target": 383931730211504128,
    "user_graph_target_name": "Herbas",
    "font_size": 2,
    "graph_user_mode": False
}

with open('config.json', 'w') as f:
    json.dump(config, f, indent=4)
    f.close()
