import json
import definitions
import subprocess
from botlog import log
import time
import datetime
import matplotlib.pyplot as plt
import numpy as np


class User:
    def __init__(self, uid):
        self.id = str(uid)
        self.mention = f"<@{self.id}>"
        self.nick_mention = f"<@!{self.id}>"
        self.total_mentions = 0
        self.messages_sent_count = 0
        self.sent_messages = []

    def add_mention(self):
        self.total_mentions += 1

    def is_mentioned(self, content):
        return (self.nick_mention in content) or (self.mention in content)

    def new_message(self):
        self.messages_sent += 1

    def fetch_msgs_set(self, chatlog):
        """
        Returns a list of the messages sent by the user
        """
        user_msgs = []
        for msg in chatlog:
            if msg["author"] == self.id:
                user_msgs.append(msg)
        self.sent_messages = user_msgs
        log(f"Fetched messages of user {self.id}")


def count_ind_authors(filename):
    log('Opening chatlog...')
    with open(f'tmp_data/{filename}', 'r') as f:
        chatlog = json.load(f)
        f.close()
    log('Succesfully loaded chatlog')

    all_authors_no_filt = []
    for message in chatlog:
        all_authors_no_filt.append(message["author"])
    log('Found all authors, removing duplicates...')

    all_authors = []
    for author in all_authors_no_filt:
        if not author in all_authors:
            all_authors.append(author)
    log('Removed duplicates, dumping...')
    with open('tmp_data/all_authors.json', 'w') as f:
        json.dump(all_authors, f)
        f.close()
    log('Dumped succesfully')


def only_in_past(in_past_limit):
    curr_time = int(time.time())
    log('Opening chatlog...')
    with open('tmp_data/chatlog.json', 'r') as f:
        chatlog = json.load(f)
        f.close()
    log('Openned chatlog.')
    new_chatlog = []
    for message in chatlog:
        if (curr_time - message["creation-time"]) < in_past_limit:
            new_chatlog.append(message)
    log('Finished evaluating messages, now dumping...')
    with open('tmp_data/recent_chatlog.json', 'w') as f:
        json.dump(new_chatlog, f)
        f.close()
    log('Dumped past month version')


def count_msgs_sent(user, chatlog):
    for msg in chatlog:
        if msg["author"] == user.id:
            user.new_message()


def count_mentions(chatlog, target_user):
    for message in chatlog:
        if (target_user.is_mentioned(message["message_content"])):
            target_user.add_mention()


def by_pings(d):
    return d['mention_total']


def by_messages(d):
    return d['messages_sent']


def sort_by(data, method):
    data = sorted(data, key=method, reverse=True)
    return data


def load_config():
    try:
        log('Attempting to load config...')
        with open('config.json', 'r') as f:
            config = json.load(f)
            f.close()
        log('Loaded config succesfully')
        return config
    except:
        from default_conf import config
        return config


class Day:
    def __init__(self, index_value):
        self.day_start = index_value
        self.day_end = index_value + definitions.DAY_LEN
        self.msg_count = 0

    def is_in_this_day(self, time):
        return (time >= self.day_start and time <= self.day_end)

    def add_message(self):
        self.msg_count += 1


def count_sent_per_day(start, end, chatlog, user_mode=False, target=0):
    day_list = []
    index = start
    while index < end:
        # new day object for every iteration
        currDay = Day(index)
        for msg in chatlog:

            # We only add the message if it is in the day
            if (currDay.is_in_this_day(msg['creation-time'])):
                if user_mode == False:
                    currDay.add_message()
                else:
                    if (str(msg['author']) == str(target)):
                        currDay.add_message()

        # This is what we will add to the list
        tmp_dict = {
            "day": str(datetime.date.fromtimestamp(currDay.day_start)),
            "message_count": currDay.msg_count
        }
        day_list.append(tmp_dict)

        # increment day
        index += definitions.DAY_LEN

        # So that running isnt boring
        log(f'Finished day {int((index - start) / definitions.DAY_LEN)}/{int((end - start) / definitions.DAY_LEN)} ')
    return day_list


def make_msg_graph(days_label, message_counts, config):
    x = (range(len(days_label)))
    new_x = [1.1*i for i in x]
    plt.bar(new_x, message_counts, 1.0)
    plt.xlabel('Date', fontsize=15)
    plt.ylabel('Messages sent', fontsize=15)
    plt.xticks(new_x, days_label, fontsize=config['font_size'], rotation=30)

    if config["graph_user_mode"] == True:
        master_title = f'Messages sent per day by {config["user_graph_target_name"]}'
        file_name = 'messages_per_day_per_user.png'
    else:
        master_title = f'Messages sent per day in the SalC1 Discord server'
        file_name = 'messages_per_day.png'

    plt.title(master_title)
    plt.savefig(file_name, dpi=config["graph_dpi"])
