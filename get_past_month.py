import json
from botlog import log
import logtools

# Length of a day
from definitions import DAY_LEN

# We need to load config for size
config = logtools.load_config()

# trim the chatlog to only include the past x days. Default is one month
logtools.only_in_past(config["get_past_count"])

# count how many individual authors
logtools.count_ind_authors(r'recent_chatlog.json')

with open('tmp_data/recent_chatlog.json', 'r') as f:
    chatlog = json.load(f)
    f.close()

with open('tmp_data/all_authors.json', 'r') as f:
    all_authors = json.load(f)
    f.close()

# get total user count for statistics
tot_usr_lenth = len(all_authors)

target_data = []

for index, uid in enumerate(all_authors):
    t_user = logtools.User(uid)
    logtools.count_msgs_sent(t_user, chatlog)
    tmp_dict = {
        "user": t_user.id,
        "messages_sent": t_user.messages_sent_count
    }
    target_data.append(tmp_dict)
    log(f'Finished user {index}/{tot_usr_lenth} ')

log('Finished all users, now sorting...')

target_data = logtools.sort_by(target_data, logtools.by_messages)

log('Sorting complete, now dumping...')

with open('tmp_data/recent_top_msgs.json', 'w') as f:
    json.dump(target_data, f)
    f.close()

log('Succesfully dumped, finished!')
