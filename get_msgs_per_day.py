import json
from botlog import log
import logtools
import datetime
import definitions

config = logtools.load_config()

log('Opening chatlog...')

with open('tmp_data/chatlog.json', 'r') as f:
    chatlog = json.load(f)
    f.close()
log('Openned chatlog.')

# end is the end of the log, start is the start. Index is what we will use to index through it
if config["graph_user_mode"]:
    # gather messages of a User
    target = logtools.User(config['user_graph_target'])
    target.fetch_msgs_set(chatlog)

    end = target.sent_messages[0]["creation-time"]
    index = start = target.sent_messages[len(
        target.sent_messages) - 1]["creation-time"]
else:
    # This is for messages sent overall
    end = chatlog[0]["creation-time"]
    start = chatlog[len(chatlog) - 1]["creation-time"]

day_list = logtools.count_sent_per_day(
    start, end, chatlog, user_mode=config["graph_user_mode"], target=config["user_graph_target"])

log('Finished all days')

# GRAPH PART

log('Loading labels and counts for graph...')

days_label = []
message_counts = []

for day in day_list:
    days_label.append(day["day"])
    message_counts.append(day["message_count"])

# If we put the days labels to close together we cant read anything
for i, j in enumerate(days_label):
    if (i % config["graph_gap"] == 0):
        days_label[i] = j
    else:
        days_label[i] = ' '

log('Loaded labels, now drawing graph...')

logtools.make_msg_graph(days_label, message_counts, config)

log('Graph completed!')
